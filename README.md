# .NET Framework Apps with Docker -> CI

Original [dockerfile source](https://github.com/sixeyed/dockerfiles-windows) 
 [Build Agent Docker image](https://hub.docker.com/u/sixeyed)

## Webinar Sign Up

An ASP.NET 4.5 WebForms app used for demonstrating Docker containers on Windows. 

## Module 1 

This is the initial state of the code, which runs on IIS and uses a remote SQL Server database.

The solution file is in Visual Studio 2017 format and contains a Wix setup project.

## Usage

To run the app you will need a SQL database to connect to 